#pragma once
#include <glm/glm/glm.hpp>

class GameObject
{
public:
	GameObject();
	~GameObject();

	virtual void update(float) = 0;
	virtual void draw(glm::mat4) = 0;

};

