#include "SceneObject.h"


//Pass in the filename for the texture
SceneObject::SceneObject(VAOData* sceneObjVAO, char* texFileName)
{
	objectVAO = sceneObjVAO;
	texture = fiLoadTexture(texFileName);
}


SceneObject::~SceneObject()
{
}