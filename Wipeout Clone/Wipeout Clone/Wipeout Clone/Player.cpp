#include <oal_ver1.1/include/alc.h>
#include <oal_ver1.1/include/al.h>
#include "Player.h"
#include "Keystates.h"


Player::Player(VAOData* playerVAO, char* texFileName, Keystate* keysTemp, ALuint soundTemp)
{
	objectVAO = playerVAO;
	texture = fiLoadTexture(texFileName);

	//pass keys from game into this container
	keys = keysTemp;

	//Pass the engine sound in (Can be done nicer)
	engineSound = soundTemp;

}

Player::~Player()
{
}


void Player::update(float deltaTime)
{
	//check key event
	checkKeyEvent();

	Object3D::update(deltaTime);

}

void Player::checkKeyEvent()
{
	if (*keys & Keys::Forward) {

		//Play sound
		ALint state;
		alGetSourcei(engineSound, AL_SOURCE_STATE, &state);
		if (AL_PLAYING != state) {
			alSourcePlay(engineSound);
		}
		
		//translate Z
		posZ += -speed * cos(thetaY);
		posX += -speed * sin(thetaY);
	}

	if (*keys & Keys::Left) {

		//rotate CCW slightly z, & slight rotation CW x
		thetaY += rotationSpeed;
	}
	if (*keys & Keys::Backward) {

		//translate Z
		posZ += backwardSpeed * cos(thetaY);
		posX += backwardSpeed * sin(thetaY);
	}
	if (*keys & Keys::Right) {

		//rotate CW slightly z, & slight rotation CW x
		thetaY -= rotationSpeed;
	}
	if (*keys & Keys::Up) {

		//translate y
		posY += 5.0f;
	}
	if (*keys & Keys::Down) {

		//translate y
		posY -= 5.0f;
	}
	if (*keys & Keys::RollL) {

		//Strafe for now - translate x
	}
	if (*keys & Keys::RollR) {

		//Strafe for now - translate x
	}
}

void Player::applyMovement(glm::vec3)
{

}
