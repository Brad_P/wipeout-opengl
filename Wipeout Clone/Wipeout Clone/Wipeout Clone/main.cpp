
#include <FreeImage\FreeImagePlus.h>
#include <CoreStructures\CoreStructures.h>
#include <iostream>
#include "main.h"
#include "Game.h"
#include "shader_setup.h"
#include "texture_loader.h"
#include "GameConfig.h"


using namespace std;
using namespace CoreStructures;

int main(int argc, char** argv) {

	//Initialise.
	init(argc, argv);

	//Set up game configurations
	gameConfig = {

		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		alcDevice,
		alcContext,
		mainShader,
		skyboxShader
	};

	initShaderUniforms();

	game = new Game(myUniforms, gameConfig);

	//Start game
	game->gameInit();

	//Initialise main loop
	glutMainLoop();

	return 0;
}

int init(int argc, char** argv) {

	glutInit(&argc, argv);
	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	//Create Window
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Wipeout Clone");

	//event handling
	glutDisplayFunc(drawFrame);

	glutMotionFunc(mouseMove);
	glutMouseFunc(mouseFunc);

	glutKeyboardFunc(handleKeyPress);
	glutKeyboardUpFunc(handleKeyUp);


	glewInit();

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	//load shaders here into the game config struct
	mainShader = setupShaders("vertexShader.shader", "fragmentShader.shader");
	//skyboxShader = setupShaders("skyboxVertexShader.shader", "skyboxFragmentShader.shader");

	//Set up sound device and context
	alcDevice = alcOpenDevice(NULL);
	alcContext = alcCreateContext(alcDevice, NULL);
	alcMakeContextCurrent(alcContext);

	return 0;
}


void initShaderUniforms() {

	gameConfig.NUM_LIGHT_PARAMS = NUM_LIGHT_PARAMS;

	//Fill struct with the uniform location to be used by objects when drawn
	myUniforms.locP = glGetUniformLocation(mainShader, "projection");
	myUniforms.projMatrix = GUMatrix4::infinitePerspectiveProjection(45.0f, 16.0f / 9.0f, 0.1f);
	myUniforms.locC = glGetUniformLocation(mainShader, "camera");
	myUniforms.locT = glGetUniformLocation(mainShader, "T");
	myUniforms.locR = glGetUniformLocation(mainShader, "R");
	myUniforms.locS = glGetUniformLocation(mainShader, "S");

	myUniforms.locNumLights = glGetUniformLocation(mainShader, "numLights");
	myUniforms.locCamPos = glGetUniformLocation(mainShader, "camPos");
	myUniforms.locShininess = glGetUniformLocation(mainShader, "shininess");

	int count = 0;
	for (int i = 0; i < myUniforms.MAX_LIGHTS * gameConfig.NUM_LIGHT_PARAMS; i++) {

		if (i == gameConfig.NUM_LIGHT_PARAMS * (count + 1)) {

			count++;
		}
		myUniforms.lightUniforms[i] = setLightUniformLocName(i - (count*gameConfig.NUM_LIGHT_PARAMS), count);
	}

}

GLuint setLightUniformLocName(int currPropertyIndex, int currLightIndex) {

	std::ostringstream ss;
	ss << "lights[" << currLightIndex << "]." << propertyNames[currPropertyIndex];
	std::string s = ss.str();
	const char * uniformName = s.c_str();

	//return uniform loc
	return glGetUniformLocation(gameConfig.mainShader, uniformName);
}

//Simple game exit, else pass forward key info to Game
void handleKeyPress(unsigned char key, int x, int y) {

	switch (key) {
	case 27:
		exit(0);
		break;
	default:
		game->keyEvent(key, x, y, true);
	}
	

}

//pass key up information to game, but send false so that key up is registered
void handleKeyUp(unsigned char key, int x, int y) {

	game->keyEvent(key, x, y, false);
}


void mouseMove(int x, int y) {
		
		game->mouseMove(x, y);
}

void mouseFunc(int button, int state, int x, int y) {

	game->mouseFunc(button, state, x, y);
}

//Draw call render to draw all VAOs to screen
 void drawFrame(void) {


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	//update game before rendering
	game->update();

	game->render();

	glutPostRedisplay();

	glutSwapBuffers();
}