#pragma once
#include <glew\glew.h>
#include <freeglut\freeglut.h>


class VAOData
{
public:
	VAOData();
	~VAOData();

	//This is the VAO of the object
	GLuint VAO = 0;

	//Set the VAO to be the one passed in by the shapeloader
	void setVAO(GLuint currentVAO) { VAO = currentVAO; }

	//return the VAO
	GLuint getVAO() { return VAO; }

	int totalVertices = 0;

	int getTotalVerts() { return totalVertices; }

	void setTotalVerts(int totalShapeVertices) { totalVertices = totalShapeVertices;  }

};

