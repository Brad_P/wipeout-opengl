#include<glew/glew.h>
#include <freeglut/freeglut.h>
#include "GameClock.h"



GameClock::GameClock()
{
	currTime = glutGet(GLUT_ELAPSED_TIME);
	prevTime = glutGet(GLUT_ELAPSED_TIME);
}


GameClock::~GameClock()
{
}

//tick over to the next frame
void GameClock::tick()
{
	deltaTime = currTime - prevTime;

	//running faster than desired fps, so delay until that time has passed
	if (deltaTime < FPS_IN_MS) {

		Sleep(FPS_IN_MS - deltaTime);
	}
	prevTime = currTime;
}
