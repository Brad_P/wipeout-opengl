#pragma once
#include <glm/glm/glm.hpp>
#include "Light.h"

//This manages the lights in the scene with an array
class SceneLighting
{
public:

	glm::vec3 camPos;
	int numLights = 0;
	const static int MAX_LIGHTS = 10;
	Light* lights[MAX_LIGHTS] = { nullptr };

	void addNewLight(Light* newLight) {

		for (int i = 0; i < MAX_LIGHTS; i++) {

			//If space found
			if (lights[i] == nullptr) {

				lights[i] = newLight;
				numLights++;
				//Light has been added, so we are done here
				return;
			}
		}
	}
};

