#pragma once
class GameClock
{
public:
	GameClock();
	~GameClock();

	void tick();

	double getDeltaTime() { return deltaTime; }

private:

	//~60fps
	const double FPS_IN_MS = 16.6f;

	double currTime;
	double prevTime;

	double deltaTime;
};

