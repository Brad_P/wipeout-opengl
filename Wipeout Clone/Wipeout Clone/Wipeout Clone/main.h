#pragma once
#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures/CoreStructures.h>
#include <oal_ver1.1/include/alc.h>
#include "Game.h"
#include "GameConfig.h"
#include "shaderUniforms.h"

Game* game;

GameConfig gameConfig;
shaderUniforms myUniforms;

ALCdevice* alcDevice;
ALCcontext* alcContext = nullptr;

GLuint mainShader;
GLuint skyboxShader;

const int SCREEN_WIDTH = 1920;
const int SCREEN_HEIGHT = 1080;

int init(int argc, char** argv);

void initShaderUniforms();

GLuint setLightUniformLocName(int, int);

void handleKeyPress(unsigned char, int, int);
void handleKeyUp(unsigned char, int, int);

void mouseMove(int, int);
void mouseFunc(int, int, int, int);

void drawFrame();

const static int NUM_LIGHT_PARAMS = 6;

char * propertyNames[NUM_LIGHT_PARAMS] =
{
"position",
"intensities",
"attenuation",
"ambientCoefficient",
"coneAngle",
"coneDirection"
};






