#include "ForceField.h"


ForceField::ForceField(VAOData * fieldVAO, char * texFileName, Object3D* tempTarget)
{
	objectVAO = fieldVAO;
	texture = fiLoadTexture(texFileName);

	target = tempTarget;

}

ForceField::~ForceField()
{
}

void ForceField::update(float deltaTime)
{
	//Set the position to be that ot the target
	setObjPos(target->getObjPos());
	setObjRot(target->getObjRot());

	Object3D::update(deltaTime);
}
