#include "ShapeLoader.h"

//This class pulls the shapes out of the .obj, so that they can each have a VAO made for them
ShapeLoader::ShapeLoader()
{
}


ShapeLoader::~ShapeLoader()
{
}

//Filename for the .obj
//If true, a VAO will be generated for every shape in the .obj
void ShapeLoader::readOBJ(std::string fileName, bool VAOPerShape)
{

	//Clear shapesVAOs array
	for (int i = 0; i < MAX_VAOS_PER_OBJ; i++) {

		currentOBJVAOs[i] = nullptr;
	}


	std::string err;
	//Tiny loader loads the important stuff for me
	bool result = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, fileName.c_str());

	if (!err.empty()) { // `err` may contain warning message.
		std::cerr << err << std::endl;
	}
	if (!result) {

		//.OBJ not found, exit application for now
		std::cout << "	.OBJ file not found\n	Exiting. . .\n";
		exit(0);
	}

	//A VAO must be created for each of the shapes, but only if there are several shapes 
	//that need seperate textures. Else, load it all as normal into one VAO

	loadShapesToVAOs(VAOPerShape, attrib, shapes, materials);
}

void ShapeLoader::loadShapesToVAOs(bool VAOPerShape, tinyobj::attrib_t attrib, std::vector<tinyobj::shape_t> shapes, std::vector<tinyobj::material_t> materials)
{
	//Counts the number of vertices iterated through so far
	int count = 0;
	int totalVertices = 0;

	//Counts the current shape being put into a VAO
	int shapeIndex = 0;

	// Loop over shapes
	for (size_t s = 0; s < shapes.size(); s++) {

		// Loop over faces(polygon)
		size_t index_offset = 0;
		for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
			int fv = shapes[s].mesh.num_face_vertices[f];

			// Loop over vertices in the face.
			for (size_t v = 0; v < fv; v++) {
				// access to vertex
				tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];

				tinyobj::real_t vx = attrib.vertices[3 * idx.vertex_index + 0];
				tinyobj::real_t vy = attrib.vertices[3 * idx.vertex_index + 1];
				tinyobj::real_t vz = attrib.vertices[3 * idx.vertex_index + 2];
				verticesXYZ[((count - totalVertices) * 3)] = vx;
				verticesXYZ[((count - totalVertices) * 3) + 1] = vy;
				verticesXYZ[((count - totalVertices) * 3) + 2] = vz;


				tinyobj::real_t nx = attrib.normals[3 * idx.normal_index + 0];
				tinyobj::real_t ny = attrib.normals[3 * idx.normal_index + 1];
				tinyobj::real_t nz = attrib.normals[3 * idx.normal_index + 2];
				normalsXYZ[((count - totalVertices) * 3)] = nx;
				normalsXYZ[((count - totalVertices) * 3) + 1] = ny;
				normalsXYZ[((count - totalVertices) * 3) + 2] = nz;

				tinyobj::real_t tx = attrib.texcoords[2 * idx.texcoord_index + 0];
				tinyobj::real_t ty = attrib.texcoords[2 * idx.texcoord_index + 1];
				texCoordsUV[((count - totalVertices) * 2)] = tx;
				texCoordsUV[((count - totalVertices) * 2) + 1] = ty;

				//counter for total vertices so far
				count++;
			}
			index_offset += fv;

			// per-face material
			shapes[s].mesh.material_ids[f];
		}

		//gen VAO if wanted for just current shape here
		if (VAOPerShape) {

			std::cout << shapes[shapeIndex].name + "\n"; //uncomment if shape list is needed
			generateVAO(shapeIndex, count - totalVertices);
			//then clear arrays for next shape VAO
			verticesXYZ[MAX_VERTICES * 3] = { 0.0f };
			normalsXYZ[MAX_VERTICES * 3] = { 0.0f };
			texCoordsUV[MAX_VERTICES * 2] = { 0.0f };
			totalVertices = count;
			shapeIndex++;
		}


	}

	totalVertices = count;

	if (!VAOPerShape) {
		
		//Shape index should be 0 here, as we are on the first and only shape
		generateVAO(shapeIndex, totalVertices);
	}
}

//Takes the index of how many shapes have been made into VAOs, and the total vertices of the current one
void ShapeLoader::generateVAO(int shapeIndex, int totalVertices)
{
	//Create a VAOData to be filled with the VAO generated for the shape/obj.
	VAOData* currentVAO = new VAOData();

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	//Create slot data for object position, slot 0
	glGenBuffers(1, &posVBO);
	glBindBuffer(GL_ARRAY_BUFFER, posVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesXYZ), verticesXYZ, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	//create slot data for normals, slot 5
	glGenBuffers(1, &normalVBO);
	glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(normalsXYZ), normalsXYZ, GL_STATIC_DRAW);
	glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(5);

	//Textures to slot 8
	glGenBuffers(1, &textureVBO);
	glBindBuffer(GL_ARRAY_BUFFER, textureVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texCoordsUV), texCoordsUV, GL_STATIC_DRAW);
	glVertexAttribPointer(8, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(8);

	glBindVertexArray(0);

	//Set the VAO of this shape/obj
	currentVAO->setVAO(VAO);
	currentVAO->setTotalVerts(totalVertices);

	//Add VAO to array of VAOs here
	currentOBJVAOs[shapeIndex] = currentVAO;

}
