#include <CoreStructures/CoreStructures.h>
#include <glm/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm/gtx/rotate_vector.hpp>
#include <oal_ver1.1/include/alc.h>
#include <oal_ver1.1/include/al.h>
#include "GURiffModel.h"
#include <mmreg.h>
#include "Game.h"
#include "VAOData.h"
#include "Keystates.h"
#include "SceneLighting.h"

using namespace CoreStructures;


Game::Game(shaderUniforms myUniformsTemp, GameConfig gameConfigTemp)
{
	//load in uniforms for shaders
	myUniforms = myUniformsTemp;

	gameConfig = gameConfigTemp;
}


Game::~Game()
{
}


void Game::gameInit() {

	//Create and start game clock
	gameClock = new GameClock();

	myUniforms.sceneLighting = new SceneLighting();

	//Attach engine sound to player
	playerEngineSound = soundInit("Player/engine.wav");


#pragma region Object Loading
	//Create the loader to handle .OBJs
	loader = new ShapeLoader();

	//read the player.obj file into one VAO
	loader->readOBJ("Player/PlayerShip.obj", false);
	//Initialise the player, with the currently loaded VAO
	player = new Player(loader->currentOBJVAOs[0], "Player/PlayerShipTexture.png", &keys, playerEngineSound);
	//place the player into objectsArray, at the first position
	objectsArray[fillCount] = player;
	fillCount++;

	//Read in the forcefield capsule
	loader->readOBJ("Player/forceField.obj", false);
	forceField = new ForceField(loader->currentOBJVAOs[0], "Player/forceFieldTexture.png", (Object3D*)player);
	transparentObjectsArray[0] = forceField;
	
	//Read the trackX.obj file, with a VAO per shape
	//Replace number with whichever track to load
	loader->readOBJ("Track1/track1.obj", true);
	for (int i = 0; i < loader->getMaxVAOs(); i++) {

		if (loader->currentOBJVAOs[i] != nullptr) {

			SceneObject* currentSceneObject = new SceneObject(loader->currentOBJVAOs[i], sceneTextures[i]);
			objectsArray[fillCount] = currentSceneObject;
			fillCount++;
		}
	}

	//Load the skybox in
	//skyBox = new SkyBox();
	//skyBox->loadCubeMap();
	//objectsArray[fillCount] = skyBox;
	//fillCount++;

#pragma endregion

#pragma region Init Lighting
	//Lights init
	Light* directLight = new Light();

	directLight->position = glm::vec4(0.0f, 0.9f, 0.0f, 0.0f);
	directLight->ambientCoefficient = 0.06f;
	directLight->intensities = glm::vec3(0.8f, 0.8f, 0.8f);

	myUniforms.sceneLighting->addNewLight(directLight);

	Light* spotLight1 = new Light();

	spotLight1->position = glm::vec4(0.0f, 15.0f, 0.0f, 1.0f);
	spotLight1->intensities = glm::vec3(150.0f, 0.0f, 0.0f);
	spotLight1->attentuation = 0.2f;
	spotLight1->coneAngle = 30.0f;
	spotLight1->coneDirection = glm::vec3(0.0f, -1.0f, 0.0f);

	myUniforms.sceneLighting->addNewLight(spotLight1);

	Light* spotLight2 = new Light();

	spotLight2->position = glm::vec4(15.0f, 15.0f, 0.0f, 1.0f);
	spotLight2->intensities = glm::vec3(0.0f, 0.0f, 100.0f);
	spotLight2->attentuation = 0.2f;
	spotLight2->coneAngle = 45.0f;
	spotLight2->coneDirection = glm::vec3(0.0f, -1.0f, 0.0f);

	myUniforms.sceneLighting->addNewLight(spotLight2);

#pragma endregion

	


	//Initialise camera, passing in the structs and the target
	camera = new MainCamera(myUniforms, gameConfig, player);


	//Initialise all 3D Objects
	for (int i = 0; i < MAX_OBJECTS; i++) {

		//check they are not empty before calling init
		if (objectsArray[i] != nullptr) {

			objectsArray[i]->objectInit(gameConfig, myUniforms);
		}
		if (transparentObjectsArray[i] != nullptr) {

			transparentObjectsArray[i]->objectInit(gameConfig, myUniforms);
		}
	}
}

ALuint Game::soundInit(char* soundFileName) {

	ALuint buffer1;
	alGenBuffers(1, &buffer1);

	auto thisSoundData = new GURiffModel(soundFileName);

	RiffChunk formatChunk = thisSoundData->riffChunkForKey(' tmf');
	RiffChunk dataChuck = thisSoundData->riffChunkForKey('atad');

	WAVEFORMATEXTENSIBLE wv;
	memcpy_s(&wv, sizeof(WAVEFORMATEXTENSIBLE), formatChunk.data, formatChunk.chunkSize);

	alBufferData(buffer1, AL_FORMAT_MONO16, (ALvoid*)dataChuck.data, (ALsizei)dataChuck.chunkSize,
		(ALsizei)wv.Format.nSamplesPerSec);

	//Create a sound source here
	ALuint source1;
	alGenSources(1, &source1);
	alSourcei(source1, AL_BUFFER, buffer1);

	//Attach to player
	alSource3f(source1, AL_POSITION, 0.0f, 0.0f, 0.0f);
	alSource3f(source1, AL_VELOCITY, 0.0f, 0.0f, 0.0f);
	alSource3f(source1, AL_DIRECTION, 0.0f, 0.0f, 0.0f);

	ALfloat listenerPos[] = { 0.0f, 0.0f, 0.0f };
	ALfloat listenerVel[] = { 0.0f, 0.0f, 0.0f };
	ALfloat listenerOri[] = { 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f };

	alListenerfv(AL_POSITION, listenerPos);
	alListenerfv(AL_VELOCITY, listenerVel);
	alListenerfv(AL_ORIENTATION, listenerOri);

	return source1;
}


void Game::update(void) {

	gameClock->tick();
	myUniforms.sceneLighting->camPos = camera->getCameraPosition();
	//update objects here
	for (int i = 0; i < MAX_OBJECTS; i++) {

		//check they are not empty before calling
		if (objectsArray[i] != nullptr) {

			objectsArray[i]->update(gameClock->getDeltaTime());
		}
		//same for transparent objects (order not important)
		if (transparentObjectsArray[i] != nullptr) {

			transparentObjectsArray[i]->update(gameClock->getDeltaTime());
		}
	}
	updateCamera();
}


//Update the camera
void Game::updateCamera(void) {

	//pass current mouse position into the camera update
	camera->update(gameClock->getDeltaTime());
	
}


#pragma region GameCallbacks
//pass the current mouse position into mouseX and Y.
void Game::mouseMove(int x, int y) {

	if (enableMouseMove) {

		camera->moveCamera(x, y);
	}
}

//Only enable the mouse to move while the mouse is clicked down
void Game::mouseFunc(int button, int state, int x, int y) {

	if ((button == GLUT_LEFT_BUTTON))
	{
		if (state == GLUT_DOWN) {
			camera->startMove(x, y);
			enableMouseMove = true;
		}

		//snap camera back to rest position
		else if (state == GLUT_UP) {

			camera->resetView();
			enableMouseMove = false;
		}

	}
}


//Handle Key press event
void Game::keyEvent(unsigned char key, int x, int y, bool keyState) {

	if (keyState) {

		switch (key) {

		case 'w':
			keys |= Keys::Forward;
			break;
		case 'W':
			keys |= Keys::Forward;
		case 'a':
			keys |= Keys::Left;
			break;
		case 'A':
			keys |= Keys::Left;
			break;
		case 's':
			keys |= Keys::Backward;
			break;
		case 'S':
			keys |= Keys::Backward;
			break;
		case 'd':
			keys |= Keys::Right;
			break;
		case 'D':
			keys |= Keys::Right;
			break;
		case 'r':
			keys |= (Keys::Up);
			break;
		case 'R':
			keys |= (Keys::Up);
			break;
		case 'f':
			keys |= (Keys::Down);
			break;
		case 'F':
			keys |= (Keys::Down);
			break;
		case 'q':
			keys |= (Keys::RollL);
			break;
		case 'Q':
			keys |= (Keys::RollL);
			break;
		case 'e':
			keys |= (Keys::RollR);
			break;
		case 'E':
			keys |= (Keys::RollR);
			break;
		default:
			break;
		}

	}
	else {

		switch (key) {

		case 'w':
			keys &= (~Keys::Forward);
			break;
		case 'W':
			keys &= (~Keys::Forward);
			break;
		case 'a':
			keys &= (~Keys::Left);
			break;
		case 'A':
			keys &= (~Keys::Left);
			break;
		case 's':
			keys &= (~Keys::Backward);
			break;
		case 'S':
			keys &= (~Keys::Backward);
			break;
		case 'd':
			keys &= (~Keys::Right);
			break;
		case 'D':
			keys &= (~Keys::Right);
			break;
		case 'r':
			keys &= (~Keys::Up);
			break;
		case 'R':
			keys &= (~Keys::Up);
			break;
		case 'f':
			keys &= (~Keys::Down);
			break;
		case 'F':
			keys &= (~Keys::Down);
			break;
		case 'q':
			keys &= (~Keys::RollL);
			break;
		case 'Q':
			keys &= (~Keys::RollL);
			break;
		case 'e':
			keys &= (~Keys::RollR);
			break;
		case 'E':
			keys &= (~Keys::RollR);
			break;
		default:
			break;
		}

	}
		
}
#pragma endregion

//loop through VAOs to draw each
void Game::render(void) {

	//Loop through all GameObject draw functions
	for (int i = 0; i < MAX_OBJECTS; i++) {

		if (objectsArray[i] != nullptr) {

			objectsArray[i]->draw(camera->getCameraMatrix());
		}
	}

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//Loop through all transparent objects
	for (int i = 0; i < MAX_OBJECTS; i++) {

		if (transparentObjectsArray[i] != nullptr) {

			transparentObjectsArray[i]->draw(camera->getCameraMatrix());
		}
	}
	glDisable(GL_BLEND);
}
