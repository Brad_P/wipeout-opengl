#pragma once
#include <FreeImage\FreeImagePlus.h>
#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include "GameObject.h"
#include "VAOData.h"
#include "tiny_obj_loader.h"
#include "texture_loader.h"
#include <CoreStructures/CoreStructures.h>
#include "GameConfig.h"
#include "shaderUniforms.h"
#include "Light.h"

class Object3D : public GameObject {

public:

	Object3D();
	~Object3D();

	//Return variables
	glm::vec3 getObjPos() { return glm::vec3(posX, posY, posZ); }
	glm::vec3 getObjRot() { return glm::vec3(thetaX, thetaY, thetaZ); }

	void setObjPos(glm::vec3 newPos) { posX = newPos.x; posY = newPos.y; posZ = newPos.z; }
	void setObjRot(glm::vec3 newRot) { thetaX = newRot.x; thetaY = newRot.y; thetaZ = newRot.z; }

	VAOData* objectVAO;

	GLuint texture;

	GameConfig gameConfig;
	shaderUniforms myUniforms;
	void objectInit(GameConfig, shaderUniforms);

	void update(float);

	void draw(glm::mat4);

	void setUniforms(glm::mat4);
	void setLightUniforms();

	//Initialise matrices
	CoreStructures::GUMatrix4 T =  CoreStructures::GUMatrix4::translationMatrix(0.0f,0.0f,0.0f);
	CoreStructures::GUMatrix4 R = CoreStructures::GUMatrix4::rotationMatrix(0.0f, 0.0f, 0.0f);
	CoreStructures::GUMatrix4 S = CoreStructures::GUMatrix4::scaleMatrix(1.0f, 1.0f, 1.0f);

protected:

	float posX = 0.0f;
	float posY = 0.0f;
	float posZ = 0.0f;

	float thetaX = 0.0f;
	float thetaY = 0.0f;
	float thetaZ = 0.0f;

	float scaleX = 1.0f;
	float scaleY = 1.0f;
	float scaleZ = 1.0f;

	float shininess = 0.0f;

};
