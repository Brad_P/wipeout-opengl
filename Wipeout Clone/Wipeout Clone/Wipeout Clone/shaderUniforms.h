#pragma once
#include <glew/glew.h>
#include <freeglut/freeglut.h>
#include <CoreStructures/CoreStructures.h>
#include "SceneLighting.h"

//This structure contains the locations of the shader uniforms.
struct shaderUniforms {

	GLuint locP;
	GLuint locC;

	CoreStructures::GUMatrix4 projMatrix;

	GLuint locT;
	GLuint locR;
	GLuint locS;

	GLuint locNumLights;
	GLuint locCamPos;
	GLuint locShininess;

	SceneLighting* sceneLighting;

	const static int NUM_LIGHT_PARAMS = 6;
	const static int MAX_LIGHTS = 10;
	GLuint lightUniforms[NUM_LIGHT_PARAMS * MAX_LIGHTS] = { 0 };

};