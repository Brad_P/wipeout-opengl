#version 330

uniform sampler2D textureImage;

uniform float shininess;
uniform vec3 camPos;

//This is the number of active lights in the scene
uniform int numLights;



#define MAX_LIGHTS 10
uniform struct Light {

	vec4 position;
	vec3 intensities;
	float attenuation;
	float ambientCoefficient;
	float coneAngle;
	vec3 coneDirection;

} lights[MAX_LIGHTS];


in packet {

	vec3 vertex;
	vec3 normal;
	vec2 texCoord;
	mat4 transform;

} inputFragment;


layout(location = 0) out vec4 fragmentColour;

vec3 applyLight(Light currLight, vec3 surfaceColor, vec3 surfacePos, vec3 surfaceToCamera) {

	vec3 surfaceToLight;
	float attenuation = 1.0f;

	if (currLight.position.w == 0.0f) {

		//Directional Light
		surfaceToLight = normalize(currLight.position.xyz);
		attenuation = 1.0f;
	}
	else {

		surfaceToLight = normalize(currLight.position.xyz - surfacePos);
		float distanceToLight = length(currLight.position.xyz - surfacePos);
		attenuation = 1.0f / (1.0f + currLight.attenuation * pow(distanceToLight, 2));

		//Cone restricts
		float lightToSurfaceAngle = degrees(acos(dot(-surfaceToLight, normalize(currLight.coneDirection))));
		if (lightToSurfaceAngle > currLight.coneAngle) {

			//Angle greater than cone range, so no light.
			attenuation = 0.0f;
		}
	}

	//Ambient effect
	vec3 ambient = currLight.ambientCoefficient * surfaceColor.rgb * currLight.intensities;

	//Diffuse effect
	float diffuseCoefficient = max(0.0f, dot(inputFragment.normal, surfaceToLight));
	vec3 diffuse = diffuseCoefficient * surfaceColor.rgb * currLight.intensities;

	//Specular effect
	float specularCoefficient = 0.0f;
	if (diffuseCoefficient > 0.0f) {

		specularCoefficient = pow(max(0.0f, dot(surfaceToCamera, reflect(-surfaceToLight, inputFragment.normal))), shininess);
	}
	vec3 specular = specularCoefficient * vec3(1, 1, 1) * currLight.intensities;

	return ambient + attenuation * (diffuse + specular);

}


void main(void) {

	//Set the initial colour as simply that from the texture.
	vec4 tempFragColour = texture2D(textureImage, inputFragment.texCoord).rgba;

	//get the normalized vec3 between the current fragment and the camera position
	vec3 surfToCam = normalize(camPos - inputFragment.vertex);

	//initialize the var to be used for adding all the lighting effects together.
	vec3 totalLightColour = vec3(0.0f, 0.0f, 0.0f);

	//Loopm through each light, adding the colour to the total colour
	for (int i = 0; i < numLights; i++) {

		totalLightColour += applyLight(lights[i], tempFragColour.xyz, inputFragment.vertex, surfToCam);

	}

	//Gamma correction
	vec3 gamma = vec3(1.0 / 2.2);

	fragmentColour = vec4(pow(totalLightColour, gamma), tempFragColour.a);
}