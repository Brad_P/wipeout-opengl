#version 330

uniform mat4 projection;
uniform mat4 camera;
uniform mat4 T;
uniform mat4 R;
uniform mat4 S;

//Input vertex packet
layout (location=0) in vec4 position;
layout (location=5) in vec3 normal;
layout (location=8) in vec2 texCoord;

out packet {

	vec3 vertex;
	vec3 normal;
	vec2 texCoord;
	mat4 transform;

}	outputVertex;


void main(void) {

	mat4 transform = T * R * S;

	mat4 invTransposeTransform = transpose(inverse(transform));

	mat3 normalMat = transpose(inverse(mat3(transform)));

	outputVertex.vertex = (transform * position).xyz;
	outputVertex.transform = transform;
	outputVertex.normal = normalize(normalMat * normal);
	outputVertex.texCoord = texCoord;
	gl_Position = projection * camera * transform * position;

	
}
