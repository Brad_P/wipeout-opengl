#pragma once
#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include "Object3D.h"
#include "ShapeLoader.h"
#include "Player.h"
#include "SkyBox.h"
#include "SceneObject.h"
#include "shaderUniforms.h"
#include "GameConfig.h"
#include "MainCamera.h"
#include "GameClock.h"
#include "Keystates.h"
#include "Light.h"
#include "ForceField.h"

//init, then update, then render

class Game
{
public:
	Game(shaderUniforms, GameConfig);
	~Game();


	void gameInit();
	ALuint soundInit(char*);

	void update(void);
	void render(void);

	//callback functions
	void mouseMove(int, int);
	void mouseFunc(int, int, int, int);
	void keyEvent(unsigned char, int, int, bool);


private:

	shaderUniforms myUniforms;
	GameConfig gameConfig;

	//Keeps track of how many objects have been put into objectsArray
	int fillCount = 0;

	Keystate keys = 0;

	GameClock* gameClock;
	MainCamera* camera;
	ShapeLoader* loader;

	Player* player;
	ForceField* forceField;

	SkyBox* skyBox;

	ALuint playerEngineSound;

	void updateCamera();

	float mouseX = 0.0f;
	float mouseY = 0.0f;

	float prevMouseX = 0.0f;
	float prevMouseY = 0.0f;


	const static int MAX_OBJECTS = 30;
	Object3D* objectsArray[MAX_OBJECTS] = { nullptr };
	Object3D* transparentObjectsArray[MAX_OBJECTS] = { nullptr };


	bool enableMouseMove = false;

	char* sceneTextures[30] = { "Track1/DarkAsphalt.jpg", "Track1/OffWhiteConcrete.jpg", "Track1/OffWhiteConcrete.jpg", "Track1/Grass.jpg"
	, "metal.jpg" , "metal.jpg" , "metal.jpg" , "metal.jpg" , "metal.jpg" , "metal.jpg" 
	, "Track1/Billboard01Texture.png" , "Track1/Billboard02Texture.png" , "metal.jpg" , "metal.jpg" , "metal.jpg" };

};
