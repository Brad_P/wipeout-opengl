#pragma once
#include "Object3D.h"
class ForceField : public Object3D
{
public:
	ForceField(VAOData* fieldVAO, char* texFileName, Object3D*);
	~ForceField();

	void update(float);

private:

	Object3D* target;
};

