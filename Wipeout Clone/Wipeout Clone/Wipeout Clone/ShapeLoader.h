#pragma once
#include <string>
#include <iostream>
#include "tiny_obj_loader.h"
#include "VAOData.h"


class ShapeLoader
{
public:
	ShapeLoader();
	~ShapeLoader();

	void readOBJ(std::string, bool);

	void loadShapesToVAOs(bool, tinyobj::attrib_t, std::vector<tinyobj::shape_t>, std::vector<tinyobj::material_t>);

	void generateVAO(int, int);

	//This is the maximum number of VAOs that can be made from one .OBJ file
	static const int MAX_VAOS_PER_OBJ = 30;
	int getMaxVAOs() { return MAX_VAOS_PER_OBJ; }

	//This is the array of VAOs pulled from the current .OBJ file
	VAOData* currentOBJVAOs[MAX_VAOS_PER_OBJ] = { nullptr };

private:

	//for tinyObj Loader
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	GLuint VAO = 0;

	//VBOs to put into the VAO
	GLuint posVBO;
	GLuint normalVBO;
	GLuint textureVBO;

	//this keeps track of the total vertices of the current shape/obj
	int totalVertices = 0;

	//Arrays of vertices, normals, textures, and index
	static const int MAX_VERTICES = 30000;
	//Stored in format X, Y, Z as pulled from .OBJ
	GLfloat verticesXYZ[MAX_VERTICES * 3] = { 0.0f };
	GLfloat normalsXYZ[MAX_VERTICES * 3] = { 0.0f };
	//Format U, V as from .OBJ
	GLfloat texCoordsUV[MAX_VERTICES * 2] = { 0.0f };

};

