#pragma once
#include <glm/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm/gtx/rotate_vector.hpp>
#include "Object3D.h"
#include "shaderUniforms.h"
#include "GameConfig.h"
#include "GameObject.h"
#include "Player.h"




class MainCamera : GameObject
{
public:
	MainCamera(shaderUniforms, GameConfig, Object3D*);
	~MainCamera();

	void update(float);
	void draw(glm::mat4);
	void moveCamera(float, float);

	Object3D* target;

	glm::mat4 getCameraMatrix() { return camera; }

	glm::vec3 getCameraPosition() { return cameraPosition; }


	//This calls when mouse down occurs to reset the mouse behaviour for the new mouse pos
	void startMove(float x, float y) {

		prevPosX = x;
		prevPosY = y;

		prevRotX = rotX;
		prevRotY = rotY;
	}

	void resetView();

private:

	glm::mat4 camera;

	shaderUniforms myUniforms;
	GameConfig gameConfig;

	//change between x and y during this frame
	float dx = 0.0f;
	float dy = 0.0f;
	
	float prevPosX = 0.0f;
	float prevPosY = 0.0f;

	float sensX = 0.005f;
	float sensY = 0.005f;

	float rotX = 0.0f;
	float rotY = 0.0f;

	float prevRotX = 0.0f;
	float prevRotY = 0.0f;



	//Initial position of camera
	glm::vec3 pivotToCam = glm::vec3(0.0f, 60.0f, 200.0f);

	glm::vec3 prevPivotToCam;

	glm::vec3 cameraPosition;

};

