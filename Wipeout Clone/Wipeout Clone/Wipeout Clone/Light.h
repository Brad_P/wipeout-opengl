#pragma once
#include <glm/glm/glm.hpp>

struct Light {

	glm::vec4 position = glm::vec4(0.0f);
	glm::vec3 intensities = glm::vec3(0.0f);
	float attentuation = 0.0f;
	float ambientCoefficient = 0.0f;
	float coneAngle = 0.0f;
	glm::vec3 coneDirection = glm::vec3(0.0f);

};
