#include <glew/glew.h>
#include <freeglut/freeglut.h>
#include <iostream>
#include "Object3D.h"
#include "MainCamera.h"




MainCamera::MainCamera(shaderUniforms myUniformsTemp, GameConfig gameConfigTemp, Object3D* targetTemp)
{
	myUniforms = myUniformsTemp;

	gameConfig = gameConfigTemp;

	target = targetTemp;

	//set initial mouse pos to screen center and hide cursor
	glutSetCursor(GLUT_CURSOR_NONE);
	glutWarpPointer(gameConfig.screenWidth / 2, gameConfig.screenHeight / 2);
}


MainCamera::~MainCamera()
{
}

//Only happens when mouse is down
void MainCamera::moveCamera(float x, float y) {

	dx = x - prevPosX;
	dy = y - prevPosY;

	rotX = prevRotX + dy;
	rotY = prevRotY + dx;

}

//Set view back to behind the player
void MainCamera::resetView() {

	glm::vec3 targetRot = target->getObjRot();

	rotX = 0.0f;
	rotY = 0.0f;

}

void MainCamera::update(float deltaTime) {



	//temporary copy of the pivot vector
	glm::vec3 tempPivotToCam = pivotToCam;

	tempPivotToCam = glm::rotateX(tempPivotToCam, rotX * sensX);
	tempPivotToCam = glm::rotateY(tempPivotToCam , (rotY * sensY) + target->getObjRot().y);

	//New camera position is the pivotal distace + the targets current position
	cameraPosition = tempPivotToCam + target->getObjPos();


	//Pass in camera location (first vec3), player pos(second vec3)
	camera = glm::lookAt(cameraPosition, target->getObjPos(), glm::vec3(0.0f, 1.0f, 0.0f));
}

//camera does not need to be drawn
void MainCamera::draw(glm::mat4 camera)
{
}
