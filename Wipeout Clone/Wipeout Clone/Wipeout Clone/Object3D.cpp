#include <string>
#include <CoreStructures/CoreStructures.h>
#include "tiny_obj_loader.h"
#include "Object3D.h"



Object3D::Object3D() {


}

Object3D::~Object3D() {


}

void Object3D::objectInit(GameConfig gameConfigTemp, shaderUniforms myUniformsTemp) {

	gameConfig = gameConfigTemp;
	myUniforms = myUniformsTemp;
}


void Object3D::update(float deltaTime) {

	//Set the transformations of the 3D object
	T = CoreStructures::GUMatrix4::translationMatrix(posX, posY, posZ);
	R = CoreStructures::GUMatrix4::rotationMatrix(thetaX, thetaY, thetaZ);
	S = CoreStructures::GUMatrix4::scaleMatrix(scaleX, scaleY, scaleZ);
}

void Object3D::draw(glm::mat4 camera) {


	glUseProgram(gameConfig.mainShader);

	//Set all of the uniforms
	setUniforms(camera);
	setLightUniforms();


	glEnable(GL_TEXTURE_2D);
	//Bind texture
	glBindTexture(GL_TEXTURE_2D, texture);

	//Bind VAO
	glBindVertexArray(objectVAO->getVAO());

	//Draw VAO
	glDrawArrays(GL_TRIANGLES, 0, objectVAO->getTotalVerts());

	glBindVertexArray(0);
	glDisable(GL_TEXTURE_2D);

}

void Object3D::setUniforms(glm::mat4 camera) {

	glUniformMatrix4fv(myUniforms.locP, 1, GL_FALSE, (GLfloat*)&myUniforms.projMatrix);
	glUniformMatrix4fv(myUniforms.locC, 1, GL_FALSE, (GLfloat*)&camera);

	glUniformMatrix4fv(myUniforms.locT, 1, GL_FALSE, (GLfloat*)&T);
	glUniformMatrix4fv(myUniforms.locR, 1, GL_FALSE, (GLfloat*)&R);
	glUniformMatrix4fv(myUniforms.locS, 1, GL_FALSE, (GLfloat*)&S);
}

void Object3D::setLightUniforms() {

	glUniform1i(myUniforms.locNumLights, myUniforms.sceneLighting->numLights);
	glUniform3f(myUniforms.locCamPos, myUniforms.sceneLighting->camPos.x, myUniforms.sceneLighting->camPos.y, myUniforms.sceneLighting->camPos.z);
	glUniform1f(myUniforms.locShininess, 10.0f);

	for (int i = 0; i < myUniforms.sceneLighting->numLights; i++) {

		glUniform4f(myUniforms.lightUniforms[i * gameConfig.NUM_LIGHT_PARAMS], myUniforms.sceneLighting->lights[i]->position.x, myUniforms.sceneLighting->lights[i]->position.y, myUniforms.sceneLighting->lights[i]->position.z, myUniforms.sceneLighting->lights[i]->position.w);
		glUniform3f(myUniforms.lightUniforms[(i * gameConfig.NUM_LIGHT_PARAMS) + 1], myUniforms.sceneLighting->lights[i]->intensities.x, myUniforms.sceneLighting->lights[i]->intensities.y, myUniforms.sceneLighting->lights[i]->intensities.z);
		glUniform1f(myUniforms.lightUniforms[(i * gameConfig.NUM_LIGHT_PARAMS) + 2], myUniforms.sceneLighting->lights[i]->attentuation);
		glUniform1f(myUniforms.lightUniforms[(i * gameConfig.NUM_LIGHT_PARAMS) + 3], myUniforms.sceneLighting->lights[i]->ambientCoefficient);
		glUniform1f(myUniforms.lightUniforms[(i * gameConfig.NUM_LIGHT_PARAMS) + 4], myUniforms.sceneLighting->lights[i]->coneAngle);
		glUniform3f(myUniforms.lightUniforms[(i * gameConfig.NUM_LIGHT_PARAMS) + 5], myUniforms.sceneLighting->lights[i]->coneDirection.x, myUniforms.sceneLighting->lights[i]->coneDirection.y, myUniforms.sceneLighting->lights[i]->coneDirection.z);

	}
	
	
}
