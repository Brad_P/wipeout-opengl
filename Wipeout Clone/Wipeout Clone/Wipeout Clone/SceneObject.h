#pragma once
#include "Object3D.h"
#include "VAOData.h"

//Scene object is any stationary, useless object that provides nothing other than scenery.
class SceneObject : public Object3D
{
public:
	SceneObject(VAOData*, char*);
	~SceneObject();
};

