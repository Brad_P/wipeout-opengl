#pragma once
#include <glew/glew.h>
#include <freeglut/freeglut.h>
#include <oal_ver1.1/include/alc.h>

//This structure contains game configurations
struct GameConfig {

	int screenWidth = 1920;
	int screenHeight = 1080;

	ALCdevice* alcDevice;
	ALCcontext* alcContext;

	GLuint mainShader;
	GLuint skyboxShader;

	int NUM_LIGHT_PARAMS = 6;
};