#pragma once
#include <glm/glm/glm.hpp>
#include <oal_ver1.1/include/alc.h>
#include <oal_ver1.1/include/al.h>
#include "GURiffModel.h"
#include <math.h>
#include "Object3D.h"
#include "VAOData.h"
#include "Keystates.h"


class Player : public Object3D
{
public:
	Player(VAOData* playerVAO, char*, Keystate*, ALuint);
	~Player();

	void update(float);
	void checkKeyEvent();

	void applyMovement(glm::vec3);

private:

	const float PI = 3.14159f;

	Keystate* keys;

	ALuint engineSound;

	const float rotationSpeed = 0.02f;

	const float speed = 10.0f;

	const float backwardSpeed = 4.0f;

};

